NODE := node

start:
	$(NODE) scripts/start.js

create_package:
	$(NODE) scripts/create_package.js
