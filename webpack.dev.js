const merge = require('webpack-merge').default;
const webpackDev = require('i2-webpack/webpacks/components/webpack.dev');

const webpackCommon = require('./webpack.common');
const { EnvironmentPlugin } = require('webpack');

const devConfig = {
    plugins: [
        new EnvironmentPlugin({
            GTM_ID: 'GTM-T95MWFG',
            GTM_AUTH: 'gAZG3q3tVYZyYkpWZzWtUA',
            GTM_PREVIEW: 'env-3',
            GTM_COOKIES_WIN: 'x',
        })
    ],
}

module.exports = (packageConfig) => merge(webpackDev, webpackCommon, packageConfig, devConfig);
