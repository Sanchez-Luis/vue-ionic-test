# Monorepo Front-end

Herramientas/Tecnologías
* **Vue JS**
    Framework para crear componentes front.
* **Webpack**
    Bundler que transpilará nuestro código ES6 a ES5
    Aqui utilizaremos un repositorio interno de Making Science que     tiene una condifuración estandar para todos los proyectos.
* **NPM**
    Gestor de dependencias.
* **Lerna JS**
    Gestión del monorepo.
* **Axios**
    Llamadas asincronas (AJAX).

## Arquitectura del proyecto
El front se gestionará con componentes web independientes, teniendo por un lado una serie de componentes dummies y un componente SPA que importará todos nuestros componentes y es donde se crearán las vistas y habrá la lógica de negocio de front (Llamadas asincronas, cambios de routes, etc..)

![alt text](./docs/monorepo-packages.jpeg "Monorepo")

### 1.- Monorepo
LernaJS nos permite gestionar ciertas operaciones sobre los componentes para poder publicarlos de forma independiente pero gestionando la configuración básica (Test, Transpilación, Linter, etc...) desde un punto y configuración común.

El siguiente repositorio es un monorepo en blanco, donde se puede clonar y utilizar como base para crear los componentes.

https://bitbucket.org/i2tic/workspace/projects/MONOREPO_FRONT

#### ¿Que componentes tendrá el monorepo?
Los componentes que debemos gestionar dentro del monorepo deberian ser componentes dummies, es decir, componentes que su responsabilidad es pintar un HTML según unas props dadas.
No deberian tener lógica de negocio, ni ser responsables de pedir información via AJAX. En el caso que necesitemos gestionar llamadas asincronas, será responsabilidad del componente root (SPA).

A demás de los componentes dummies, existirá un componente root (SPA), que contendrá las vistas de la aplicación e importará los componentes dummies necesarios por cada vista. En este punto es donde tendremos la responsabilidad de ir obteniendo los datos necesarios para cada vista de forma asincrona.

### 2.- Proyecto Django
Aqui es donde se instalará el componente del monorepo que contenta toda la aplicación (SPA).


### Comandos básicos del monorepo


#### npm start
Cuando trabajamos con monorepo, podemos levantar cada componente de forma independiente desde el root.

En este caso, el `npm start` necesita una variable de entorno para saber cual componente queremos levantar. Para este caso usaremos la variable `NAME` donde indicaremos el nombre del directorio de nuestro componente.

Ejemplo: `NAME="header" npm start`


#### npm run lint
Se analizan todos los ficheros JS para comprobar que se ha mantenido el estandar y reglas definidas en el fichero `.eslintrc`
Se utiliza el estandar de Airbnb sobreescribiendo ciertas reglas.

#### npm run stylelint
Comprueba que los ficheros de estilos (SASS) estén respetando las reglas del linter.

Se pueden agregar reglas en el fichero `.stylelintrc.json`


#### npm run test:ci
Se ejecutará todos los test unitarios de TODOS los componentes

#### npm run test:only-no-report
Muchas veces cuando hacemos test, queremos ejecutar un test en concreto, para evitar que se ejecuten todos los test de todos los componentes y se genere la cobertura de código (que tarda un tiempo considerable según la cantidad de componentes y test), podemos ejecutar este comando donde se buscarán los test que en su definición tenga la palaba `current_test`
#### npm run bootstrap
Muchas veces tendremos componentes del monorepo que tienen como dependencia otro componente del mismo monorepo, donde queremos poder trabajar en nuestro entorno local pudiendo transpilar al guardar cada cambio dentro del monorepo. Con este comando, lernajs crea un enlace simbolico y linka los componentes entre sí para poder ver cambios en tiempo real.


## Cómo publicar nuevos cambios de un componente.
Una vez que terminamos de trabajar sobre algun componente(s) del monorepo, debemos generar una versión nueva que contenga nuestro cambios, para asi poder instalarla en nuestro proyecto final.

Para eso tenemos los siguientes comandos:
#### npm run release:major
Lo usaremos cuando los cambios que hemos hecho en un componente rompen la compatibilidad con la versión anterior.

#### npm run release:minor
Cuando se trata de mejoras, nuevas funcionalidades del componente

#### npm run release:patch
Cuando hemos arreglado un bug.

> Cada uno de estos comandos, lernaJS detecta qué componentes se han modificado desde la última vez que se generó una versión y nos mostrará una lista de todos los componentes que van a aumentar la versión. Esto es muy útil ya que si tenemos un componente A que tiene como dependencia un componente B y ambos son parte del monorepo, si modificamos el código de B y generamos versión, lernaJS aumentará la versión de B y también el de A.

## Cómo mockear peticiones AJAX con webpack
Muchas veces nuestra aplicación necesita comunicarse con el servidor para obtener ciertos datos de forma asincrona via AJAX y no queremos acoplar nuestro desarrollo de front al back.

En estos casos podemos mockear cada peticion AJAX para que nos devuelva ciertos resultados y asi trabajar en nuestro front de forma independiente al back.

Para ellos debemos modificar el webpack.dev.js de nuestro componente SPA y agregar la configuración `devServer` a webpack (Ver la documentación de webpack)

Ejemplo
```javascript
devServer: {
    before: (app) => {
        app.get('/ajax/products/', (_, response) => {
            const products = [
                { name: 'Product 1', id: 1 },
                { name: 'Product 2', id: 2 },
                { name: 'Product 3', id: 3 },
            ];

            setTimeout(() => {
            // Utilizamos el setTimeout para simular el tiempo de respuesta del back.
                response.json(menu);
            }, 1500);
        });
    }
},
```
De esta forma nuestra aplicación Front, cada vez que haga una peticion de tipo `GET` a la url `/ajax/products/` responderá con el listado de productos definidos.

## Anatomia de un componente
Cada componente tiene la siguiente estructura de ficheros y directorios:

```
example/
    index.html
    index.js
src/
    core/
    styles/
    app.html
    app.js
    App.vue
test/
    App.test.js
```

#### Example/
Este directorio se usa solo al desarrollar de forma local. instancia el componente en un HTML y asi podemos desarrollar sobre el componente de una forma totalmente agnostica al proyecto donde se va a utilizar.

#### Src/
En este directorio están los ficheros core del componente.
- app.html: Contiene todo el HTML del componente
- app.js: Especifica el nombre del componente e importa el `index.js` del directorio core/
- App.vue: Importa el HTML,SCSS y JS del componente-

#### Src/core/
En este directorio tendremos la lógica del componente.

Vue tiene su propio ciclo de vida donde debemos exportar un objeto que contenga ciertas claves (data, methods, componentes, props, watch, etc..) cada uno de ellos estará en un fichero separado, y el `index.js` los importará todos. Ejemplo:
```
core/
    index.js
    methods.js
    data.js
    props.js
```

Siendo el contenido de `index.js`
```javascript
import props from './props';
import data from './data';
import methods from './methods';

export default {
    data,
    props,
    methods,
};
```