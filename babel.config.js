const presets = [
    '@vue/app',
    [
        '@babel/preset-env',
        {
            useBuiltIns: false,
        },
    ],
];

const plugins = [
    'add-module-exports',
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    ['@babel/proposal-class-properties', { loose: true }],
    '@babel/plugin-transform-runtime',
    '@babel/plugin-transform-regenerator',
    '@babel/plugin-transform-async-to-generator',
    '@babel/plugin-transform-modules-commonjs',
];

module.exports = {
    presets,
    plugins,
};
