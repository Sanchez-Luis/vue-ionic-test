const path = require('path');

module.exports = {
    resolve: {
        alias: {
            '@globalHelpers': path.resolve(__dirname, 'helpers/'),
            '@api': path.resolve(__dirname, 'api/'),
        },
    }
};
