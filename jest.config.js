const hasToShowReport = !process.env.REPORT;

module.exports = {
    setupFilesAfterEnv: ['<rootDir>/test/setup.js'],
    collectCoverage: hasToShowReport,
    coverageDirectory: 'coverage/',
    coverageReporters: ['lcov', 'text'],
    collectCoverageFrom: [
        'packages/**/src/**/*.{js,vue}',
        '<rootDir>/test/*.test.js',
        '!packages/**/src/**/app.js',
        '!packages/**/src/**/core/props.js',
        '!packages/**/src/**/core/index.js',
        '!packages/**/src/**/core/components.js',
        '!packages/**/src/**/core/methods.js', // eliminar linea para test coverage
    ],
    coverageThreshold: {
        global: {
            statements: 75,
            branches: 75,
            functions: 75,
            lines: 75,
        },
    },
    testMatch: [
        '<rootDir>/packages/**/test/**/*test.js',
        '<rootDir>/test/**/*test.js',
        '!packages/**/src/**/core/props.js',
        '!packages/**/src/**/core/index.js',
        '!packages/**/src/**/core/components.js',
    ],
    snapshotSerializers: [
        'jest-serializer-vue',
    ],
    transform: {
        '^.+\\.(js|jsx)$': 'babel-jest',
        '^.+\\.vue$': 'vue-jest',
        '.+\\.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
    },
    moduleFileExtensions: [
        'js',
        'jsx',
        'json',
        'vue',
    ],
    verbose: true,
    transformIgnorePatterns: ['/node_modules/(?!(vuetify*))'],
    moduleNameMapper: {
        '^@globalHelpers(.*)': '<rootDir>/helpers/$1',
        '^@api(.*)': '<rootDir>/api/$1',
    },
    modulePathIgnorePatterns: ['<rootDir>/packages/app'],
};
