const fs = require('fs');
const path = require('path');
const { spawn } = require('child_process');

const WEBPACK_DEV_FILE_NAME = 'webpack.dev.js';

const findWebpackConfig = (componentDirectory) => {
    let webpackFile = null;

    fs.readdirSync(componentDirectory).forEach((directory) => {
        const customDirectory = path.resolve(componentDirectory, directory);

        const stat = fs.statSync(customDirectory);

        if (stat.isFile() && customDirectory.endsWith(WEBPACK_DEV_FILE_NAME)) {
            webpackFile = customDirectory;
        }
    });

    return webpackFile;
};

const runWebpackDev = () => spawn(
    'webpack-dev-server',
    ['--config', WEBPACK_DEV_FILE_NAME],
    { env: { ...process.env } },
);

const clonePackage = () => spawn(
    'git',
    ['clone', 'git@bitbucket.org:i2tic/package-boilerplate.git', `tmp/${process.env.NAME}`],
);

module.exports = {
    clonePackage,
    findWebpackConfig,
    runWebpackDev,
};
