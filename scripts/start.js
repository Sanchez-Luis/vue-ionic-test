const path = require('path');

const { findWebpackConfig, runWebpackDev } = require('./helpers/files');

const componentName = process.env.NAME;

if (!(componentName)) {
    console.log('Missing package name');
    console.log('You should specified a NAME env.');
    process.exit();
}

const componentPath = path.resolve(`./packages/${componentName}`);

process.chdir(componentPath);

const webpackFilePath = findWebpackConfig(componentPath);

if (!(webpackFilePath)) {
    console.log(`Missing webpack file for ${componentName} package.`);
    process.exit();
}

const webpackDev = runWebpackDev();

webpackDev.stdout.on('data', (data) => {
    console.log(`stdout: ${data}`);
});

webpackDev.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
});

webpackDev.on('error', (error) => {
    console.log(`error: ${error.message}`);
});

webpackDev.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
});
