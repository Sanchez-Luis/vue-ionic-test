// const path = require('path');
const { clonePackage } = require('./helpers/files');

const componentName = process.env.NAME;

if (!(componentName)) {
    console.log('Missing package name');
    console.log('You should specified a NAME env.');
    process.exit();
}

const cloneCommand = clonePackage();

cloneCommand.stdout.on('data', (data) => {
    console.log(`stdout: ${data}`);
});

cloneCommand.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
});

cloneCommand.on('error', (error) => {
    console.log(`error: ${error.message}`);
});

cloneCommand.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
});
