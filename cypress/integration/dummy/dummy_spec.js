describe('Dummy component', () => {
    beforeEach(() => {
        cy.open();
    });

    describe('When page is loaded', () => {
        it('Should render an h1 element with default text', () => {
            cy.get('h1').should('have.text', 'Click here (0 times)');
        });
    });

    describe('When click on h1 element', () => {
        it('The text changes by increasing its value by 1', () => {
            cy.get('h1').should('have.text', 'Click here (0 times)');

            cy.get('h1').click();

            cy.get('h1').should('have.text', 'Click here (1 times)');
        });
    });
});
