const merge = require('webpack-merge').default;
const webpackProd = require('i2-webpack/webpacks/components/webpack.prod');

const webpackCommon = require('./webpack.common');

module.exports = (packageConfig) => merge(webpackProd, webpackCommon, packageConfig);
