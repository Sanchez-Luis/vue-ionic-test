/* eslint-disable import/no-extraneous-dependencies */
import renderHelper from '@globalHelpers/render';
import ExampleImage from './static/img/logo.png';

import App from '../src/App.vue';

const props = {
    image: ExampleImage,
    btnText: 'You don’t have orders',
    btnAction: () => console.log('boton'),
    clickLogo: () => console.log('click en el logo'),
};

renderHelper(App, {
    elementToRender: '#i2tic-example',
    props,
});
