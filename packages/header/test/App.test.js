/* eslint-env node, jest */
/* eslint-disable import/no-extraneous-dependencies */
import { mount } from '@vue/test-utils';

import App from '../src/App.vue';

// const TIME_OUT = 500;

describe('Header', () => {
    let initialProps;
    let wrapper;

    beforeEach(() => {
        initialProps = {
            image: '',
            btnText: 'You don’t have any active orders',
            btnAction: jest.fn(() => console.log('boton')),
            clickLogo: jest.fn(() => console.log('logo')),
            btnState: true,
        };

        wrapper = mount(App, {
            propsData: {
                ...initialProps,
            },
        });
    });

    it('HTML Snapshot', () => {
        expect(wrapper.html()).toMatchSnapshot();
    });

    describe('When component is loaded', () => {
        it('Then render an Button element with the text comming from props', () => {
            const buttonElement = wrapper.find('.btn-outline');
            expect(buttonElement.text()).toBe('You don’t have any active orders');
        });

        it('Then render an Button element, check if is active', () => {
            const buttonElement = wrapper.findAll('.btn-outline-red');
            expect(buttonElement.exists()).toBe(true);
        });
    });

    describe('When click on button', () => {
        it('Check if emit a console log', () => {
            const buttonelement = wrapper.findAll('.btn-outline');
            buttonelement.trigger('click');
            expect(wrapper.props().btnAction.mock.calls.length).toEqual(1);
        });
    });
});
