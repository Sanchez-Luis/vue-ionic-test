# HEADER Component project #

* Header vue component

## Requirements ##

* Node version >= 6.2

* Install dependencies

```
npm i
```

* Start the development. This will command will start the development server builds, automatic testing and linting.

```bash
NAME="header" npm start # This command should be execute in monorepo root path
```
* Open ```http://localhost:8080/``` in a browser.


## How to use ##
```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Header component</title>
    <link rel="stylesheet" href="OUR_TRANSPILED_STYLES.css" />
</head>

<body>
    <div id="i2tic-header"></div>
</body>

<script src="__dist__/OUR_TRANSPILED_JS.js"></script>

</html>
```
```javascript
import Header from '@i2tic-project-name/header';


new Vue({
        render: renderElement => renderElement(Header, { props }),
    }).$mount('#i2tic-header');

```
