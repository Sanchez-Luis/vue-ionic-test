export default {
    image: {
        type: String,
        required: true,
    },
    btnText: {
        type: String,
        required: true,
    },
    btnState: {
        type: Boolean,
        default: false,
    },
    btnAction: {
        type: Function,
    },
    clickLogo: {
        type: Function,
    },
};
