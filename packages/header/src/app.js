import componentConfig from './core';

export default {
    name: 'Header',
    ...componentConfig,
};
