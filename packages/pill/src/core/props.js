export default {
    size: {
        type: String,
        default: 'normal',
    },
    buttonText: {
        type: String,
        default: '',
    },
    value: {
        type: String,
        default: '',
    },
    badge: {
        type: String,
        default: '',
    },
    isActive: {
        Boolean,
        default: false,
    },
    idBtn: {
        type: String,
        default: '',
    },
};
