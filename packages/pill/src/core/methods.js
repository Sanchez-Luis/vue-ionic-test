function onClickButton() {
    this.$emit('getvalue', this.isActive ? '' : this.value);
}

export default {
    onClickButton,
};
