import componentConfig from './core';

export default {
    name: 'Pill',
    ...componentConfig,
};
