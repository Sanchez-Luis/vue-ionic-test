/* eslint-env node, jest */
/* eslint-disable import/no-extraneous-dependencies */
import { mount } from '@vue/test-utils';

import App from '../src/App.vue';

describe('Pill', () => {
    let initialProps;
    let wrapper;

    beforeEach(() => {
        initialProps = {

        };

        wrapper = mount(App, {
            propsData: {
                ...initialProps,
            },
        });
    });

    it('HTML Snapshot', () => {
        expect(wrapper.html()).toMatchSnapshot();
    });

    it('Button is rendered', () => {
        expect(wrapper.findAll('.btn').length).toBe(1);
    });

    it('Emits click when called and send value', async () => {
        wrapper.vm.$emit('getvalue');
        wrapper.vm.$emit('getvalue', '10:00 - 10:30');
        await wrapper.vm.$nextTick();
        expect(wrapper.emitted().getvalue).toBeTruthy();
        expect(wrapper.emitted().getvalue[1]).toEqual(['10:00 - 10:30']);
    });

    it('Class active add when button is clicked', async () => {
        const props = {
            isActive: true,
        };

        wrapper = mount(App, {
            propsData: {
                ...props,
            },
        });
        expect(wrapper.findAll('.active').length).toBe(1);
    });

});
