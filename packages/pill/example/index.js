/* eslint-disable import/no-extraneous-dependencies */
import renderHelper from '@globalHelpers/render';

import App from '../src/App.vue';

const props = {
    buttonText: '10:00 - 10:30',
    size: 'md',
    value: '10:00 - 10:30',
    badge: '10',
    onClick: () => {
        console.log('clicking');
    },
};

renderHelper(App, {
    elementToRender: '#i2tic-example',
    props,
});
