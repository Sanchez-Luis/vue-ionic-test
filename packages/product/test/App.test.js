/* eslint-env node, jest */
/* eslint-disable import/no-extraneous-dependencies */
import { mount } from '@vue/test-utils';

import App from '../src/App.vue';

describe('Product', () => {
    let initialProps;
    let wrapper;

    beforeEach(() => {
        initialProps = {
            image: '',
            dataProduct: {
                name: 'TestTitle',
                price: 6,
                quantity: 0,
            },
            handleChange: jest.fn((result) => {
                console.log('this', this.dataProduct);
                initialProps.dataProduct.quantity = result;
            }),
        };

        wrapper = mount(App, {
            propsData: {
                ...initialProps,
            },
        });
    });

    it('HTML Snapshot', () => {
        expect(wrapper.html()).toMatchSnapshot();
    });

    describe('When component is loaded and image not provided', () => {
        it('Then render an H4 element with text comming from props', () => {
            const h4Element = wrapper.find('h4');
            expect(h4Element.text()).toBe('TestTitle');
        });

        it('Then render Price number of product with the price comming from props', () => {
            const pElement = wrapper.find('p');
            expect(pElement.text()).toBe('6 €');
        });
    });

    describe('When component is loaded and image is provided', () => {
        it('Render image coming from props', () => {
            const dataProduct = {
                name: 'TestTitle',
                price: 6,
                quantity: 0,
                picture: 'exampleImageNotEmpty',
            };
            initialProps = {
                ...initialProps,
                dataProduct,
            };
            wrapper = mount(App, {
                propsData: {
                    ...initialProps,
                },
            });

            console.log(wrapper.props());
            expect(wrapper.props().dataProduct.picture).toBe('exampleImageNotEmpty');

        });
    });

});
