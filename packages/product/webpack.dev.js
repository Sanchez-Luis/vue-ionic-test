const webpackDevelopmentConfig = require('../../webpack.dev');

module.exports = webpackDevelopmentConfig({
    entry: {
        index: ['./example/index.js'],
    },
    output: {
        library: 'Product',
    },
    devtool: 'source-map',
});
