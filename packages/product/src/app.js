import componentConfig from './core';

export default {
    name: 'Product',
    ...componentConfig,
};
