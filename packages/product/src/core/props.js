export default {
    dataProduct: {
        type: Object,
        default: () => ({
            name: 'Default',
            price: 0,
            picture: '',
            quantity: 0,
        }),
    },
    handleChange: {
        type: Function,
    },
    disableBtn: {
        type: Boolean,
        default: false,
    },
};
