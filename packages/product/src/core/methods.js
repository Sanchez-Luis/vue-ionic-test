function addquantity() {
    const product = {
        ...this.dataProduct,
        quantity: this.dataProduct.quantity + 1,
    };
    this.handleChange(product);
}

function subquantity() {
    const product = {
        ...this.dataProduct,
        quantity: this.dataProduct.quantity === 0 ? 0 : this.dataProduct.quantity - 1,
    };
    this.handleChange(product);
}

export default {
    addquantity,
    subquantity,
};
