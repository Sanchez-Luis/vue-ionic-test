/* eslint-disable import/no-extraneous-dependencies */
import renderHelper from '@globalHelpers/render';

import App from '../src/App.vue';

import ExampleImage from './static/img/exampleProduct.jpg';

const props = {
    dataProduct: {
        title: 'Pasta',
        price: 6,
        quantity: 0,
        pciture: ExampleImage,
    },
    handleChange: (result) => {
        props.dataProduct.quantity = result;
    },
};

renderHelper(App, {
    elementToRender: '#i2tic-example',
    props,
});
