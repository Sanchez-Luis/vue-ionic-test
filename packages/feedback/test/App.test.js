/* eslint-env node, jest */
/* eslint-disable import/no-extraneous-dependencies */
import { mount } from '@vue/test-utils';

import App from '../src/App.vue';

const TIME_OUT = 500;

describe('Feedback', () => {
    let initialProps;
    let wrapper;

    beforeEach(() => {
        initialProps = {
            title: 'Quest',
        };

        wrapper = mount(App, {
            propsData: {
                ...initialProps,
            },
        });
    });

    it('HTML Snapshot', () => {
        expect(wrapper.html()).toMatchSnapshot();
    });

    describe('When component is loaded', () => {
        it('Then render an H5 element with default text', () => {
            const h1Element = wrapper.find('h5');
            expect(h1Element.text()).toBe('Quest');
        });
    });

    describe('When click in image element', () => {

        it('Emits click when called and send value', async () => {
            wrapper.vm.$emit('getvalue');
            wrapper.vm.$emit('getvalue', 1);
            await wrapper.vm.$nextTick();
            expect(wrapper.emitted().getvalue).toBeTruthy();
            expect(wrapper.emitted().getvalue[1]).toEqual([1]);
        });

        it('The text changes by increasing its value by 1', (done) => {
            const imageElements = wrapper.findAll('.icon');
            imageElements.at(0).trigger('click');
            setTimeout(() => {
                expect(wrapper.findAll('.selected').length).toBe(1);
                done();
            }, TIME_OUT);
        });
    });
});
