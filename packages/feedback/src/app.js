import componentConfig from './core';

export default {
    name: 'Feedback',
    ...componentConfig,
};
