import FeedbackDissatisfied from '../static/img/feedback_dissatisfied.svg';
import FeedbackHappy from '../static/img/feedback_happy.svg';
import FeedbackNeutral from '../static/img/feedback_neutral.svg';
import FeedbackSad from '../static/img/feedback_sad.svg';
import FeedbackSatisfied from '../static/img/feedback_satisfied.svg';

export default () => ({
    ratingImage: [
        FeedbackSad,
        FeedbackDissatisfied,
        FeedbackNeutral,
        FeedbackSatisfied,
        FeedbackHappy,
    ],
    selection: null,
});
