function handleSelection(index) {
    this.selection = index;
    this.$emit('getvalue', this.selection);
}

export default {
    handleSelection,
};
