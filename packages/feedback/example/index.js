/* eslint-disable import/no-extraneous-dependencies */
import renderHelper from '@globalHelpers/render';

import App from '../src/App.vue';

const props = {
    title: 'Ejemplo Pregunta 1',
};

renderHelper(App, {
    elementToRender: '#i2tic-example',
    props,
});
