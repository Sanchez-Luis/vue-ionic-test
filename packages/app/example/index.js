/* eslint-disable import/no-extraneous-dependencies */
import renderHelper from '@globalHelpers/render';

import App from '../src/App.vue';
import router from '../src/routes';
import store from '../src/store';
import Gtm from '../src/gtm/gtmConfig';
import Cookies from '../src/gtm/cookiesConfig';

renderHelper(App, {
    elementToRender: '#i2tic-example',
    router,
    store,
    Gtm,
    Cookies,
});
