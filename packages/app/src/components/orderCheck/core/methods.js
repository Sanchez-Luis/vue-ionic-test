function handleClick(action) {
    this.handleAction({
        id: this.data.id,
        operation: action,
    });
}

function handleEditBtn(data) {
    this.handleEdit(data);
}

function openModal() {
    this.modal = true;
}

function handleCloseModal() {
    this.modal = false;
}

export default {
    handleClick,
    openModal,
    handleCloseModal,
    handleEditBtn,
};
