export default {
    data: {
        type: Object,
        required: true,
    },
    handleAction: {
        type: Function,
    },
    handleStatus: {
        type: Function,
    },
    handleEdit: {
        type: Function,
    },
};
