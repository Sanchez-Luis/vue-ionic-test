export default {
    statusPickUp() {
        return this.handleStatus(this.data);
    },
    total() {
        let count = 0;
        for (let index = 0; index < this.data.items.length; index += 1) {
            count += parseFloat(this.data.items[index].price, 10)
                    * parseInt(this.data.items[index].quantity, 10);
        }
        return count.toFixed(2);
    },
    typeNotification() {
        const status = {
            confirmed: {
                title: 'Active',
                icon: this.Cutlery,
                color: 'primary',
            },
            pickup: {
                title: 'Picked up',
                icon: this.HandWithPlate,
                color: 'secondary',
            },
            editable: {
                title: 'Need attention',
                icon: this.WarningCircle,
                color: 'danger',
            },
            closed: {
                title: 'Closed',
                icon: this.Cutlery,
                color: 'secondary',
            },
        };

        return status[this.data.status];
    },
};
