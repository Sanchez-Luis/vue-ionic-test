import WarningCircle from '../static/img/warning_circle.svg';
import Cutlery from '../static/img/cutlery.svg';
import HandWithPlate from '../static/img/hand_with_plate.svg';

export default () => ({
    visible: false,
    modal: false,
    WarningCircle,
    Cutlery,
    HandWithPlate,
});
