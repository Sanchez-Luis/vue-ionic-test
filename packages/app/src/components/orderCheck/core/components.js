import {
    BRow,
    BCol,
    BIcon,
    BIconChevronDown,
    BCollapse,
    BButton,
    BFormTextarea,
    BBadge,
    BImg,
} from 'bootstrap-vue';

import ProductDetail from '@i2tic-ikeafood/product-detail';
import ModalConfirmation from '../../modalConfirmation/modalConfirmation.vue';

export default {
    BRow,
    BCol,
    BIcon,
    BIconChevronDown,
    BCollapse,
    ProductDetail,
    BButton,
    BFormTextarea,
    ModalConfirmation,
    BBadge,
    BImg,
};
