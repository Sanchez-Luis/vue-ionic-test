import componentConfig from './core';

export default {
    name: 'TimeList',
    ...componentConfig,
};
