import componentConfig from './core';

export default {
    name: 'BackButton',
    ...componentConfig,
};
