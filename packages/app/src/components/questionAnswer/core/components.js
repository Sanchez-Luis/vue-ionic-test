import {
    BRow,
    BCol,
    BIcon,
    BIconChevronDown,
    BIconChevronUp,
    BCollapse,
    BButton,
} from 'bootstrap-vue';

export default {
    BRow,
    BCol,
    BIcon,
    BIconChevronDown,
    BIconChevronUp,
    BCollapse,
    BButton,
};
