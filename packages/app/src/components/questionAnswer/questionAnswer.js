import componentConfig from './core';

export default {
    name: 'QuestionAnswer',
    ...componentConfig,
};
