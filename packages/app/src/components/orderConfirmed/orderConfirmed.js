import componentConfig from './core';

export default {
    name: 'OrderConfirmed',
    ...componentConfig,
};
