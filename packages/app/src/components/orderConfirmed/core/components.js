import {
    BContainer,
    BRow,
    BCol,
    BButton,
    BImg,
} from 'bootstrap-vue';

export default {
    BContainer,
    BRow,
    BCol,
    BButton,
    BImg,
};
