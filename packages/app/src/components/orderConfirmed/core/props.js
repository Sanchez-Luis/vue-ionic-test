export default {
    handleClick: {
        type: Function,
    },
    image: {
        type: String,
    },
    message: {
        type: String,
    },
};
