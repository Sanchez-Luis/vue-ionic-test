import componentConfig from './core';

export default {
    name: 'FilterCategory',
    ...componentConfig,
};
