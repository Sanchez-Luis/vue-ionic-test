export default {
    data: {
        type: Array,
    },
    handleFilter: {
        type: Function,
    },
    multiselect: {
        type: Boolean,
        default: false,
    },
};
