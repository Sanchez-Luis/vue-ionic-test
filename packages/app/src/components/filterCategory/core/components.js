import Pill from '@i2tic-ikeafood/pill';
import { Hooper, Slide } from 'hooper';

export default {
    Pill,
    Hooper,
    Slide,
};
