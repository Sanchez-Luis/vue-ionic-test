function handleSelectCategory(value, index) {
    if (value !== '') {
        this.selected = this.multiselect ? { ...this.selected, [index]: value } : { [index]: value };
    } else {
        delete this.$delete(this.selected, index);
    }
    this.handleFilter(this.selected);
}

export default {
    handleSelectCategory,
};
