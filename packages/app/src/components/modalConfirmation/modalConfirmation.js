import componentConfig from './core';

export default {
    name: 'ModalConfirmation',
    ...componentConfig,
};
