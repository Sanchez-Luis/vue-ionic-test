import components from './components';
import data from './data';
import methods from './methods';
import props from './props';

export default {
    components,
    data,
    props,
    methods,
    watch: {
        showModal(newVal) {
            this.modalShow = newVal;
        },
    },
};
