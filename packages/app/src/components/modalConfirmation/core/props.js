export default {
    showModal: {
        type: Boolean,
        default: false,
    },
    title: {
        type: String,
    },
    handleYes: {
        type: Function,
    },
};
