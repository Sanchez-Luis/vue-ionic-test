function sendClose() {
    this.$emit('close');
}

export default {
    sendClose,
};
