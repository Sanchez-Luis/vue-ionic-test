import BackButton from '../../backButton/backButton.vue';
import LogOutButton from '../../logOutButton/logOutButton.vue';

export default {
    BackButton,
    LogOutButton,
};
