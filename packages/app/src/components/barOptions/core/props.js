export default {
    handleBack: {
        type: Function,
    },
    disabledBack: {
        type: Boolean,
        default: false,
    },
};
