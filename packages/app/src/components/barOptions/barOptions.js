import componentConfig from './core';

export default {
    name: 'BarOptions',
    ...componentConfig,
};
