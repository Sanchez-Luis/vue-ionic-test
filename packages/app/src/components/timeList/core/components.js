import {
    BRow, BCol,
} from 'bootstrap-vue';
import Pill from '@i2tic-ikeafood/pill';

export default {
    Pill,
    BRow,
    BCol,
};
