function handleSelectTime(value, index) {
    this.indexTime = value !== '' ? index : null;
    this.handleChange(value, index);
}

export default {
    handleSelectTime,
};
