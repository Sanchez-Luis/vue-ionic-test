export default {
    data: {
        type: Array,
        require: true,
    },
    handleChange: {
        type: Function,
    },
    noBadge: {
        type: Boolean,
        default: false,
    },
};
