import { clearAccessToken, clearRefreshToken } from '../../../services/localStorage/jwt';
import { redirectToLogOut } from '../../../Token/jwt';

function handleLogOut() {
    clearAccessToken();
    clearRefreshToken();
    redirectToLogOut();
}

export default {
    handleLogOut,
};
