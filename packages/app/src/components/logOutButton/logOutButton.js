import componentConfig from './core';

export default {
    name: 'LogOutButton',
    ...componentConfig,
};
