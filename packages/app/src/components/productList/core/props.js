export default {
    data: {
        type: Array,
        require: true,
    },
    handleChange: {
        type: Function,
    },
    disableAction: {
        type: Boolean,
        default: false,
    },
};
