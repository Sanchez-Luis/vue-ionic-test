import {
    BRow, BCol,
} from 'bootstrap-vue';
import Product from '@i2tic-ikeafood/product';

export default {
    BRow,
    BCol,
    Product,
};
