import componentConfig from './core';

export default {
    name: 'ProductList',
    ...componentConfig,
};
