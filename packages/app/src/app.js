import componentConfig from './core';

export default {
    name: 'App',
    ...componentConfig,
};
