import VueGtm from '@gtm-support/vue2-gtm';
import Vue from 'vue';

Vue.use(VueGtm, {
    id: process.env.GTM_ID,
    queryParams: {
        gtm_auth: process.env.GTM_AUTH,
        gtm_preview: process.env.GTM_PREVIEW,
        gtm_cookies_win: process.env.GTM_COOKIES_WIN,
    },
    enabled: false,
});
