import componentConfig from './core';

export default {
    name: 'Cookies',
    ...componentConfig,
};
