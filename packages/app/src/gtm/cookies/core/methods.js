function closeBtn() {
    this.visible = !this.visible;
}

function setCookies() {
    this.visible = true;
    this.$cookies.set(this.cookiesName, 1, Infinity);
    this.$gtm.enable(true);
}

export default {
    closeBtn,
    setCookies,
};
