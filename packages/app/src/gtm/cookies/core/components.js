import {
    BRow,
    BContainer,
    BCol,
    BButton,
} from 'bootstrap-vue';

export default {
    BRow,
    BCol,
    BButton,
    BContainer,
};
