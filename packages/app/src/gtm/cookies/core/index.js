import components from './components';
import data from './data';
import methods from './methods';
import props from './props';

export default {
    components,
    data,
    methods,
    props,
    created() {
        if (this.$cookies.isKey(this.cookiesName)) {
            if (this.$cookies.get(this.cookiesName) === '1') {
                this.visible = true;
                this.$gtm.enable(true);
            }
        }
    },
};
