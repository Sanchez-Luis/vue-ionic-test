import componentConfig from './core';

export default {
    name: 'MainLayouts',
    ...componentConfig,
};
