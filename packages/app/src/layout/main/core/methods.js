function goToActiveOrders() {
    this.$router.push(this.URL_CONSTANT.activeOrders);
}

function goToHomePage() {
    if (this.$route.path !== '/') {
        this.$router.push(this.URL_CONSTANT.home);
    }
}

export default {
    goToActiveOrders,
    goToHomePage,
};
