import {
    BContainer, BRow, BCol, BImg, BButton, BSpinner,
} from 'bootstrap-vue';
import Header from '@i2tic-ikeafood/header';
import Cookies from '../../../gtm/cookies/cookies.vue';

export default {
    BContainer,
    BRow,
    BCol,
    BImg,
    BButton,
    Header,
    BSpinner,
    Cookies,
};
