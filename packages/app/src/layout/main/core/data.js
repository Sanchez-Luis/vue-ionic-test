import Logo from '../../../static/img/logo.png';
import URL_CONSTANT from '../../../routes/urlConstant';

export default () => ({
    logoSrc: Logo,
    URL_CONSTANT,
});
