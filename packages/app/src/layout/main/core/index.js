import components from './components';
import computed from './computed';
import methods from './methods';
import data from './data';

export default {
    components,
    computed,
    methods,
    data,
    async created() {
        this.$store.dispatch('fetchHeader');
    },
    // watch: {
    //     '$route' () {
    //         if (this.$route.path === '/') {
    //             this.colorNav = true
    //         }
    //     }
    // }
};
