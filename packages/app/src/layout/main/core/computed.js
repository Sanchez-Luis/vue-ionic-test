import { mapGetters } from 'vuex';

export default {
    ...mapGetters([
        'doneCountActiveOrders',
        'doneFetchStatus',
    ]),
    handleOrderText() {
        return this.doneCountActiveOrders > 1 ? 'orders' : 'order';
    },
};
