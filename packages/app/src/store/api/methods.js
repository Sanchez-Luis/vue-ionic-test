/* eslint-disable consistent-return */
/**
 * Common methods to make AJAX calls
 */
import axios from 'axios';

import { getAccessToken } from '../../services/localStorage/jwt';

function getCommonHeaders() {
    return {
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            Accept: 'application/json',
        },
    };
}

function getCommonAuthHeaders() {
    return {
        ...getCommonHeaders(),
        Authorization: `Bearer ${getAccessToken()}`,
    };
}

axios.interceptors.request.use((config) => {
    // Do something before request is sent
    let newHeaders = null;

    if (config.headers.isAuth) {
        newHeaders = getCommonAuthHeaders();
        delete newHeaders.isAuth;
    } else {
        newHeaders = getCommonHeaders();
    }

    return { ...config, headers: newHeaders };
}, (error) => {
    // Do something with request error
    Promise.reject(error);
});

export default {
    /**
     * Makes a GET request to the API.
    */
    get: async (url, params = {}, responseConfig = {}) => {
        try {
            const response = await axios.get(
                url,
                { params, ...responseConfig },
            );

            return response.data;
        } catch (error) {
            throw error.response;
        }
    },

    /**
     * Makes a POST request to the API.
 */
    post: async (url, data, responseConfig = {}) => {
        try {
            const response = await axios.post(
                url,
                { ...data },
                responseConfig,
            );

            return response.data;
        } catch (error) {
            throw error.response;
        }
    },

    /**
     * Makes a PUT request to the API.
 */
    put: async (url, data, responseConfig = {}) => {
        try {
            const response = await axios.put(
                url,
                { ...data },
                responseConfig,
            );

            return response.data;
        } catch (error) {
            throw error.response;
        }
    },
    /**
     * Makes a Patch request to the API.
 */
    patch: async (url, data, responseConfig = {}) => {
        try {
            const response = await axios.patch(
                url,
                { ...data },
                responseConfig,
            );

            return response.data;
        } catch (error) {
            throw error.response;
        }
    },

    /**
     * Makes a DELETE request to the API.
 */
    delete: async (url, responseConfig = {}) => {
        try {
            const response = await axios.delete(
                url,
                responseConfig,
            );

            return response.data;
        } catch (error) {
            throw error.response;
        }
    },
};
