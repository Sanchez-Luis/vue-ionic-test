import methods from './methods';

const VERIFY_URL = '/api/token/verify/';
const REFRESH_URL = '/api/token/refresh/';

export default {
    verifyJWT: (token, endpoint = VERIFY_URL) => methods.post(endpoint, { token }),
    refreshJWT: (refresh, endpoint = REFRESH_URL) => methods.post(endpoint, { refresh }),
};
