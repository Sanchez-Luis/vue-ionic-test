import methods from './methods';

const BASE_URL = '/api/menu_day/:day/';

export default {
    getAll: (endpoint = BASE_URL) => methods.get(endpoint, {}, { headers: { isAuth: true } }),
};
