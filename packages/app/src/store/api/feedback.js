import methods from './methods';

const CREATE_FEEDBACK = '/api/create_feedback/';

export default {
    createfeedback: (data, endpoint = CREATE_FEEDBACK) => methods.post(endpoint, data, { headers: { isAuth: true } }),
};
