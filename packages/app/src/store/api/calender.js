import methods from './methods';

const BASE_URL = '/api/calendar/';

export default {
    get: (endpoint = BASE_URL) => methods.get(endpoint, {}, { headers: { isAuth: true } }),
};
