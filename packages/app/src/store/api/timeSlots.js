import methods from './methods';

const BASE_URL = '/api/time_slots/:day/';

export default {
    getAll: (endpoint = BASE_URL) => methods.get(endpoint, {}, { headers: { isAuth: true } }),
};
