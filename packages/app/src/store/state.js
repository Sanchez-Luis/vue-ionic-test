export default {
    // Time Variables
    timeList: [],
    selectionDay: '',
    selectioDayPrint: '',
    timeSelected: '',
    takeaway: false,
    // Product variables
    productList: [],
    productSelected: [],
    filterProductList: [],
    productCount: 0,
    // Active Orders
    activeOrders: [],
    countActiveOrders: 0,
    // store
    storeId: null,
    openingTime: '',
    closingTime: '',
    // Categories
    categories: [],
    // Others
    selectionAllowed: true,
    // Calender
    calender: [],
    // Fecth
    fetchStatus: false,
    // Edit
    editStatus: false,
    editIdOrder: null,
    editObservation: '',
};
