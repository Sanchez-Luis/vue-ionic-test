/* eslint-disable max-statements */
/* eslint-disable import/no-cycle */
import TimeSlots from './api/timeSlots';
import MenuDay from './api/menuDay';
import Orders from './api/orders';
import Header from './api/header';
import Categories from './api/categories';
import Feedback from './api/feedback';
import Calender from './api/calender';
import Helpers from '../helpers/index';

async function fetchTimes({ commit, state }) {
    try {
        commit('SET_CURRENT_FETCH_STATUS', true);
        const timeSlots = await TimeSlots.getAll(`/api/time_slots/${state.selectionDay}/`);
        if (timeSlots) {
            commit('SET_CURRENT_TIME_LIST', timeSlots.time_slots);
            commit('SET_CURRENT_SELECTION_DAY_PRINT', timeSlots.selection_day_print);
            commit('SET_CURRENT_FETCH_STATUS', false);
        }
    } catch (error) {
        commit('SET_CURRENT_FETCH_STATUS', false);
        console.log(error);
    }
}

async function setTime({ commit }, payload) {
    commit('SET_CURRENT_TIME_SELECTED', payload);
}

async function setSelectionDay({ commit }, payload) {
    commit('SET_CURRENT_SELECTION_DAY', payload);
}

function setTakeaway({ commit }, payload) {
    commit('SET_CURRENT_TAKEAWAY', payload);
}

async function fetchProduct({ commit, state, dispatch }) {
    try {
        commit('SET_CURRENT_FETCH_STATUS', true);
        const productList = await MenuDay.getAll(`/api/menu_day/${state.selectionDay}/`);
        if (productList) {
            commit('SET_CURRENT_STORE_ID', productList.store.id);
            dispatch('setProductList', productList.items);
            commit('SET_CURRENT_FETCH_STATUS', false);
        }
    } catch (error) {
        commit('SET_CURRENT_FETCH_STATUS', false);
        console.log(error);
    }
}

async function setProductList({ commit }, payload) {
    commit('SET_CURRENT_PRODUCT_LIST', payload);
}

function filterProduct({ commit, state }, payload) {
    const newArray = state.productList.filter((each) => {
        let condition = false;
        for (let index = 0; index < payload.length; index += 1) {
            if (each.category.toString() === payload[index]) {
                condition = true;
            }
        }
        return condition;
    });
    commit('SET_CURRENT_FILTER_PRODUCT_LIST', newArray);
}

function countProduct({ commit }, payload) {
    let count = 0;
    for (let index = 0; index < payload.length; index += 1) {
        count += payload[index].quantity;
    }
    commit('SET_CURRENT_PRODUCT_COUNT', count);
}

async function setProducts({ commit }, payload) {
    commit('SET_CURRENT_PRODUCT_SELECTED', payload);
}

// eslint-disable-next-line max-statements
async function createOrder({ state, dispatch, commit }, payload) {
    try {
        commit('SET_CURRENT_FETCH_STATUS', true);
        const hours = state.timeSelected.split('-');
        const data = {
            id_store: state.storeId,
            items: state.productSelected,
            observations: payload,
            selection_day: state.selectionDay,
            hour_from: hours[0].trim(),
            hour_to: hours[1].trim(),
            take_away: state.takeaway,
        };
        dispatch('setProductList', []);
        const result = await Orders.createOrder(data);
        if (result) {
            dispatch('fetchHeader');
            commit('SET_CURRENT_FETCH_STATUS', true);
            return { status: true };
        }
    } catch (error) {
        commit('SET_CURRENT_FETCH_STATUS', false);
        return { status: false, errorMessage: error.data.errors[0] };
    }
    commit('SET_CURRENT_FETCH_STATUS', false);
    return { status: false, errorMessage: "The order can't be completed" };
}

async function fetchActiveOrders({ commit }) {
    try {
        commit('SET_CURRENT_FETCH_STATUS', true);
        const result = await Orders.activeOrders();
        if (result) {
            commit('SET_CURRENT_ACTIVE_ORDERS', result);
            commit('SET_CURRENT_FETCH_STATUS', false);
        }
    } catch (error) {
        commit('SET_CURRENT_FETCH_STATUS', false);
        console.log(error);
    }
}

async function updateOrder({ dispatch, commit }, payload) {
    try {
        commit('SET_CURRENT_FETCH_STATUS', true);
        const urlSamples = '/api/update_order/:id/';
        const updateEndpoint = urlSamples.replace(':id', payload.id);
        const response = await Orders.updateOrder(
            payload.data,
            updateEndpoint,
        );

        if (response) {
            dispatch('fetchHeader');
            commit('SET_CURRENT_FETCH_STATUS', false);
            return true;
        }
        commit('SET_CURRENT_FETCH_STATUS', false);
        return false;
    } catch (error) {
        commit('SET_CURRENT_FETCH_STATUS', false);
        return false;
    }
}

// eslint-disable-next-line max-statements
async function fetchHeader({ commit }) {
    try {
        commit('SET_CURRENT_FETCH_STATUS', true);
        const response = await Header.get();
        if (response) {
            commit('SET_CURRENT_SELECTION_ALLOWED', response.selection_allowed);
            commit('SET_CURRENT_COUNT_ACTIVE_ORDERS', response.num_active_orders);
            commit('SET_CURRENT_OPENING_TIME', response.opening_time);
            commit('SET_CURRENT_CLOSING_TIME', response.closing_time);
            commit('SET_CURRENT_FETCH_STATUS', false);
        }
    } catch (error) {
        commit('SET_CURRENT_FETCH_STATUS', false);
        console.log(error);
    }
}

async function fetchCategories({ commit }) {
    try {
        commit('SET_CURRENT_FETCH_STATUS', true);
        const response = await Categories.get();
        if (response) {
            commit('SET_CURRENT_CATEGORIES', response);
            commit('SET_CURRENT_FETCH_STATUS', false);
        }
    } catch (error) {
        commit('SET_CURRENT_FETCH_STATUS', false);
        console.log(error);
    }
}

async function createFeedback({ dispatch, commit }, payload) {
    try {
        commit('SET_CURRENT_FETCH_STATUS', true);
        const result = await Feedback.createfeedback(payload);
        if (result) {
            dispatch('fetchHeader');
            commit('SET_CURRENT_FETCH_STATUS', false);
            return { status: true };
        }
    } catch (error) {
        console.log(error);
    }
    commit('SET_CURRENT_FETCH_STATUS', false);
    return { status: false };
}

async function fetchCalender({ commit }) {
    try {
        commit('SET_CURRENT_FETCH_STATUS', true);
        const result = await Calender.get();
        if (result) {
            commit('SET_CURRENT_CALENDER', result);
            commit('SET_CURRENT_FETCH_STATUS', false);
        }
    } catch (error) {
        commit('SET_CURRENT_FETCH_STATUS', false);
        console.log(error);
    }
}

async function editOrder({ commit, dispatch }, payload) {
    const {
        day,
        id,
        items,
        observations,
        // eslint-disable-next-line camelcase
        time_slot,
    } = payload;

    const formatSelectionDay = Helpers.getDateFormat(new Date(day), true);
    const selectedItems = items.filter((each) => each.availability);

    dispatch('setEditStatus', true);
    commit('SET_CURRENT_SELECTION_DAY', formatSelectionDay);
    commit('SET_CURRENT_SELECTION_DAY_PRINT', day);
    commit('SET_CURRENT_EDIT_ID_ORDER', id);
    commit('SET_CURRENT_PRODUCT_SELECTED', selectedItems);
    commit('SET_CURRENT_TIME_SELECTED', time_slot);
    commit('SET_CURRENT_EDIT_OBSERVATION', observations);
    return true;
}

async function modifyActualOrder({ commit, state, dispatch }, payload) {
    try {
        commit('SET_CURRENT_FETCH_STATUS', true);
        const urlOrder = '/api/modify_order/:id/';
        const updateEndpoint = urlOrder.replace(':id', state.editIdOrder);
        const data = {
            items: state.productSelected,
            observations: payload,
        };
        const response = await Orders.modifyOrder(data, updateEndpoint);

        if (response) {
            dispatch('fetchHeader');
            commit('SET_CURRENT_FETCH_STATUS', true);
            return { status: true };
        }

    } catch (error) {
        commit('SET_CURRENT_FETCH_STATUS', false);
        return { status: false, errorMessage: error.data.errors[0] };
    }
    commit('SET_CURRENT_FETCH_STATUS', false);
    return { status: false, errorMessage: "The order can't be completed" };
}

async function setEditStatus({ commit }, payload) {
    commit('SET_CURRENT_EDIT_STATUS', payload);
}

function editListProduct({ state, dispatch }) {
    const newProductList = Helpers.fuseTwoObject({
        obj1: state.productList,
        obj2: state.productSelected,
        valueComparate: 'id',
        valueToMerge: 'quantity',
    });
    dispatch('setProductList', newProductList);
}

function cleanStepper({ dispatch }) {
    dispatch('setTime', '');
    dispatch('setSelectionDay', '');
    dispatch('setProductList', []);
    dispatch('filterProduct');
    dispatch('setProducts', []);
}

export default {
    fetchTimes,
    setTime,
    fetchProduct,
    setProducts,
    createOrder,
    fetchActiveOrders,
    updateOrder,
    fetchHeader,
    fetchCategories,
    filterProduct,
    createFeedback,
    countProduct,
    fetchCalender,
    setSelectionDay,
    setProductList,
    setTakeaway,
    editOrder,
    editListProduct,
    setEditStatus,
    cleanStepper,
    modifyActualOrder,
};
