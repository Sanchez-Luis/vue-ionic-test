/* eslint-disable  no-param-reassign */
/* eslint-disable  no-return-assign */
const SET_CURRENT_TIME_LIST = (state, payload) => state.timeList = payload;
const SET_CURRENT_SELECTION_DAY = (state, payload) => state.selectionDay = payload;
const SET_CURRENT_SELECTION_DAY_PRINT = (state, payload) => state.selectioDayPrint = payload;
const SET_CURRENT_TIME_SELECTED = (state, payload) => state.timeSelected = payload;
const SET_CURRENT_PRODUCT_LIST = (state, payload) => {
    const productList = payload.map((each) => {
        if (!each.quantity) {
            return { ...each, quantity: 0 };
        }
        return each;
    });
    state.productList = productList;
};
const SET_CURRENT_PRODUCT_SELECTED = (state, payload) => state.productSelected = payload;
const SET_CURRENT_STORE_ID = (state, payload) => state.storeId = payload;
const SET_CURRENT_ACTIVE_ORDERS = (state, payload) => state.activeOrders = payload;
const SET_CURRENT_COUNT_ACTIVE_ORDERS = (state, payload) => state.countActiveOrders = payload;
const SET_CURRENT_SELECTION_ALLOWED = (state, payload) => state.selectionAllowed = payload;
const SET_CURRENT_OPENING_TIME = (state, payload) => state.openingTime = payload;
const SET_CURRENT_CLOSING_TIME = (state, payload) => state.closingTime = payload;
const SET_CURRENT_CATEGORIES = (state, payload) => state.categories = payload;
const SET_CURRENT_FILTER_PRODUCT_LIST = (state, payload) => state.filterProductList = payload;
const SET_CURRENT_PRODUCT_COUNT = (state, payload) => state.productCount = payload;
const SET_CURRENT_CALENDER = (state, payload) => state.calender = payload;
const SET_CURRENT_FETCH_STATUS = (state, payload) => state.fetchStatus = payload;
const SET_CURRENT_TAKEAWAY = (state, payload) => state.takeaway = payload;
const SET_CURRENT_EDIT_ID_ORDER = (state, payload) => state.editIdOrder = payload;
const SET_CURRENT_EDIT_STATUS = (state, payload) => state.editStatus = payload;
const SET_CURRENT_EDIT_OBSERVATION = (state, payload) => state.editObservation = payload;

export default {
    SET_CURRENT_TIME_LIST,
    SET_CURRENT_SELECTION_DAY,
    SET_CURRENT_TIME_SELECTED,
    SET_CURRENT_PRODUCT_LIST,
    SET_CURRENT_PRODUCT_SELECTED,
    SET_CURRENT_SELECTION_DAY_PRINT,
    SET_CURRENT_STORE_ID,
    SET_CURRENT_ACTIVE_ORDERS,
    SET_CURRENT_COUNT_ACTIVE_ORDERS,
    SET_CURRENT_SELECTION_ALLOWED,
    SET_CURRENT_CLOSING_TIME,
    SET_CURRENT_OPENING_TIME,
    SET_CURRENT_CATEGORIES,
    SET_CURRENT_FILTER_PRODUCT_LIST,
    SET_CURRENT_PRODUCT_COUNT,
    SET_CURRENT_CALENDER,
    SET_CURRENT_FETCH_STATUS,
    SET_CURRENT_TAKEAWAY,
    SET_CURRENT_EDIT_ID_ORDER,
    SET_CURRENT_EDIT_STATUS,
    SET_CURRENT_EDIT_OBSERVATION,
};
