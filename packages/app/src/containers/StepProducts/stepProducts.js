import componentConfig from './core';

export default {
    name: 'StepProduct',
    ...componentConfig,
};
