import components from './components';
import data from './data';
import methods from './methods';
import props from './props';
import computed from './computed';

export default {
    components,
    data,
    methods,
    props,
    computed,
    async created() {
        if (this.$store.getters.doneProductList.length === 0) {
            await this.$store.dispatch('fetchProduct');
            await this.$store.dispatch('fetchCategories');
        }

        if (this.$store.getters.doneEditStatus) {
            this.$store.dispatch('editListProduct');
        }
        this.productList = this.$store.getters.doneProductList;
        this.productSelected = [...this.$store.getters.doneProductSelected];
        this.$store.dispatch('countProduct', this.productList);
    },
};
