import { mapActions } from 'vuex';

function handleChangeProduct(product, index) {
    this.handleQuantity(product, index);

    const isSelected = checkIsProductSelected(product, this.productSelected);

    if (product.quantity === 0 && isSelected.status) {
        this.productSelected = this.productSelected.filter((each) => each.id !== product.id);
    } else if (isSelected.status) {
        this.productSelected[isSelected.index] = product;
    } else if (product.quantity > 0) {
        this.productSelected.push(product);
    }
}

function handleQuantity(product, index) {
    if (this.doneFilterProductList.length > 0 || this.filterList.length > 0) {
        this.doneFilterProductList[index].quantity = product.quantity;
    } else {
        this.productList[index].quantity = product.quantity;
    }
    this.countProduct(this.productList);
}

function checkIsProductSelected(product, arrayProduct) {
    let reponse = {
        status: false,
        index: null,
    };
    for (let index = 0; index < arrayProduct.length; index += 1) {
        if (arrayProduct[index].id === product.id) {
            reponse = {
                status: true,
                index,
            };
            break;
        }
    }
    return reponse;
}

function handleClick() {
    this.setProducts(this.productSelected);
    this.filterProduct([]);
    if (this.$gtm.enabled()) {
        this.$gtm.trackEvent({
            products: this.productSelected,
        });
    }
    this.next();
}

function handleFilterProduct(filterCategory) {
    this.filterList = Object.values(filterCategory);
    this.filterProduct(this.filterList);
}

function cardClick() {
    if (this.doneProductCount > 0) {
        this.handleClick();
    }
}

export default {
    handleChangeProduct,
    handleClick,
    handleFilterProduct,
    handleQuantity,
    cardClick,
    ...mapActions([
        'filterProduct',
        'setProducts',
        'countProduct',
    ]),

};
