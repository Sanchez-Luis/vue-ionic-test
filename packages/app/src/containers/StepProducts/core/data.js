import ShoppingLogo from '../static/img/gift_bag.svg';

export default () => ({
    productSelected: [],
    productList: [],
    filterList: {},
    ShoppingLogo,
});
