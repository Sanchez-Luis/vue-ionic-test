import { mapGetters } from 'vuex';

export default {
    ...mapGetters([
        'doneProductList',
        'doneCategories',
        'doneFilterProductList',
        'doneProductCount',
        'doneEditStatus',
    ]),
    handleTitleText() {
        return this.doneEditStatus ? '1. Update your meal' : '3. Select your meal';
    },
};
