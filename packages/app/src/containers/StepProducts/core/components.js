import {
    BContainer,
    BRow,
    BCol,
    BButton,
    BImg,
} from 'bootstrap-vue';

import ProductList from '../../../components/productList/productList.vue';
import BackButton from '../../../components/backButton/backButton.vue';
import FilterCategory from '../../../components/filterCategory/filterCategory.vue';

export default {
    ProductList,
    BContainer,
    BRow,
    BCol,
    BButton,
    BackButton,
    FilterCategory,
    BImg,
};
