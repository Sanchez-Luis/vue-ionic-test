import componentConfig from './core';

export default {
    name: 'FAQ',
    ...componentConfig,
};
