import {
    BContainer,
    BRow,
    BCol,
    BButton,
    BCollapse,
} from 'bootstrap-vue';

import BarOption from '../../../components/barOptions/barOptions.vue';
import QuestionAnswer from '../../../components/questionAnswer/questionAnswer.vue';

export default {
    BContainer,
    BRow,
    BCol,
    BButton,
    BCollapse,
    BarOption,
    QuestionAnswer,
};
