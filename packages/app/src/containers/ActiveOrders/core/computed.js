import { mapGetters } from 'vuex';

export default {
    ...mapGetters([
        'doneActiveOrders',
        'doneOpeningTime',
        'doneClosingTime',
    ]),
};
