import { mapActions } from 'vuex';
import helpers from '../../../helpers';

async function handleAction({ id, operation }) {
    const sendData = {
        id,
        data: {
            operation,
        },
    };
    const response = await this.updateOrder(sendData);

    if (response) {
        this.$router.push(this.URL_CONSTANT.feedback);
    }
}

function handleStatus(order) {
    if (order.status === 'editable') {
        return false;
    }

    const orderDay = helpers.getDateFormat(new Date(order.day));
    const actualDay = helpers.getDateFormat(new Date());
    const insideStoreTime = actualTimeInterval(this.doneOpeningTime, this.doneClosingTime);

    if (orderDay === actualDay && insideStoreTime) {
        return false;
    }

    return true;
}

function actualTimeInterval(open, closed) {
    const openTime = new Date(`${helpers.getDateFormat(new Date())} ${open}`);
    const closedTime = new Date(`${helpers.getDateFormat(new Date())} ${closed}`);
    const actualTime = new Date();

    if (actualTime > openTime && actualTime < closedTime) {
        return true;
    }

    return false;
}

function handleBack() {
    this.$router.push(this.URL_CONSTANT.home);
}

function handleEditOrer(order) {
    const response = this.editOrder(order);
    if (response) {
        this.$router.push(this.URL_CONSTANT.stepper);
    }
}

export default {
    ...mapActions([
        'updateOrder',
        'editOrder',
    ]),
    handleAction,
    handleStatus,
    handleBack,
    handleEditOrer,
};
