import {
    BContainer, BRow, BCol,
} from 'bootstrap-vue';

import OrderCheck from '../../../components/orderCheck/orderCheck.vue';
import BarOption from '../../../components/barOptions/barOptions.vue';

export default {
    OrderCheck,
    BContainer,
    BRow,
    BCol,
    BarOption,
};
