import componentConfig from './core';

export default {
    name: 'ActiveOrders',
    ...componentConfig,
};
