import { mapGetters } from 'vuex';

export default {
    handleBackProductList() {
        if (this.doneEditStatus && this.actualStep === 3) {
            return true;
        }
        return false;
    },
    ...mapGetters([
        'doneEditStatus',
    ]),
};
