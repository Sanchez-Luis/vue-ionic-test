import components from './components';
import data from './data';
import methods from './methods';
import computed from './computed';

export default {
    components,
    data,
    methods,
    computed,
    created() {
        if (this.actualStep === 1 && this.URL_CONSTANT.stepperDay !== this.$route.path) {
            this.$router.push(this.URL_CONSTANT.stepperDay);
        }
        if (this.doneEditStatus) {
            this.actualStep = 3;
        }
    },
    mounted() {
        window.onhashchange = () => {
            this.handleUrlChange(this.$route.path);
        };
    },
    beforeDestroy() {
        this.setEditStatus(false);
        this.cleanStepper();
    },
};
