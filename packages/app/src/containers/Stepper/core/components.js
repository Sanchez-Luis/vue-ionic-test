import {
    BContainer,
    BRow,
    BCol,
} from 'bootstrap-vue';
import StepDay from '../../StepDay/stepDay.vue';
import StepTime from '../../StepTime/stepTime.vue';
import StepProducts from '../../StepProducts/stepProducts.vue';
import StepCart from '../../StepCart/stepCart.vue';
import OrderConfirmed from '../../../components/orderConfirmed/orderConfirmed.vue';
import BarOption from '../../../components/barOptions/barOptions.vue';

export default {
    StepTime,
    StepProducts,
    StepCart,
    OrderConfirmed,
    BContainer,
    BRow,
    BCol,
    BarOption,
    StepDay,
};
