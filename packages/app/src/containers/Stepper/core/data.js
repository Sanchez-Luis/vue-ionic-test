import URL_CONSTANT from '../../../routes/urlConstant';
import imgSuccess from '../../../static/img/success.png';

export default () => ({
    actualStep: 1,
    URL_CONSTANT,
    orderMessage: 'Your order is confirmed!',
    orderImage: imgSuccess,
});
