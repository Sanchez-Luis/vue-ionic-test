import { mapActions } from 'vuex';
import ErrorImage from '../../../static/img/error.png';

function nextStep() {
    this.actualStep += 1;
}

function backStep() {
    this.actualStep -= 1;
}

function handleNext() {
    switch (this.actualStep) {
    case 1:
        this.$router.push(this.URL_CONSTANT.stepperTime);
        break;
    case 2:
        this.$router.push(this.URL_CONSTANT.stepperMeal);
        break;
    case 3:
        this.$router.push(this.URL_CONSTANT.stepperCart);
        break;
    case 4:
        this.$router.push(this.URL_CONSTANT.stepperFinal);
        break;
    default:
        break;
    }
    return this.nextStep();
}

function handleUrlChange() {
    this.$router.push(this.URL_CONSTANT.home);
}

function handleBack() {
    switch (this.actualStep) {
    case 1:
        this.$router.push(this.URL_CONSTANT.home);
        break;
    case 2:
        this.$router.push(this.URL_CONSTANT.stepperDay);
        break;
    case 3:
        this.$router.push(this.URL_CONSTANT.stepperTime);
        break;
    case 4:
        this.$router.push(this.URL_CONSTANT.stepperMeal);
        break;
    default:
        break;
    }
    return this.backStep();
}

async function creationOfOrder(observations) {
    const response = await this.createOrder(observations);
    if (!response.status) {
        this.orderMessage = response.errorMessage;
        this.orderImage = ErrorImage;
    }
    return this.handleNext();
}

async function editOrder(observations) {
    const response = await this.modifyActualOrder(observations);
    this.orderMessage = 'Your order have been updated!';
    if (!response.status) {
        this.orderMessage = response.errorMessage;
        this.orderImage = ErrorImage;
    }
    return this.handleNext();
}

function finalStep() {
    this.$router.push(this.URL_CONSTANT.home);
}

export default {
    nextStep,
    backStep,
    finalStep,
    creationOfOrder,
    handleBack,
    handleNext,
    handleUrlChange,
    ...mapActions([
        'createOrder',
        'setEditStatus',
        'cleanStepper',
        'modifyActualOrder',
    ]),
    editOrder,
};
