import componentConfig from './core';

export default {
    name: 'Stepper',
    ...componentConfig,
};
