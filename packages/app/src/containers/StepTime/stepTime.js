import componentConfig from './core';

export default {
    name: 'StepTime',
    ...componentConfig,
};
