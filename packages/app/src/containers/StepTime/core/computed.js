import { mapGetters } from 'vuex';

export default {
    ...mapGetters([
        'doneTimeList',
        'doneSelectionDayPrint',
    ]),
};
