import components from './components';
import data from './data';
import methods from './methods';
import computed from './computed';
import props from './props';

export default {
    components,
    data,
    methods,
    computed,
    props,
    async created() {
        await this.$store.dispatch('fetchTimes');
    },
    watch: {
        takeaway() {
            this.timeSelected = '';
        },
    },
};
