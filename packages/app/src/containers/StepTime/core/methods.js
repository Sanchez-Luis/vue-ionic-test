import { mapActions } from 'vuex';

function handleChangeTimeValue(value, index) {
    this.timeSelected = value;
    this.timeIndex = index;
}

function handleClick() {
    this.setTime(this.timeSelected);
    this.setTakeaway(this.takeaway);
    this.setProductList([]);
    this.filterProduct();
    if (this.$gtm.enabled()) {
        this.$gtm.trackEvent({
            time_interval: this.timeSelected,
            available_seats: this.doneTimeList[this.timeIndex].free_seats,
            take_away: this.takeaway,
        });
    }
    this.next();
}

export default {
    ...mapActions([
        'setTime',
        'setProductList',
        'filterProduct',
        'setTakeaway',
    ]),
    handleChangeTimeValue,
    handleClick,
};
