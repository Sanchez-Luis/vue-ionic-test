import {
    BContainer,
    BRow,
    BCol,
    BButton,
    BFormCheckbox,
} from 'bootstrap-vue';

import TimeList from '../../../components/timeList/timeList.vue';

export default {
    TimeList,
    BContainer,
    BRow,
    BCol,
    BButton,
    BFormCheckbox,
};
