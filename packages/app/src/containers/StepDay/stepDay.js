import componentConfig from './core';

export default {
    name: 'StepDay',
    ...componentConfig,
};
