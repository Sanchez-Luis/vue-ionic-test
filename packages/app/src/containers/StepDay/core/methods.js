import { mapActions } from 'vuex';
import helpers from '../../../helpers';

function dateDisabled(ymd, date) {
    return this.getIfDate(date).response;
}

function getIfDate(date) {
    let response = true;
    let currentIndex = null;
    if (this.doneCalender.length > 0) {
        for (let index = 0; index < this.doneCalender.length; index += 1) {
            const availableDayDate = new Date(this.doneCalender[index].day);
            if (helpers.getDateFormat(date) === helpers.getDateFormat(availableDayDate)) {
                response = false;
                currentIndex = index;
            }
        }
    }
    return { response, index: currentIndex };
}

function dateClass(ymd, date) {
    const resp = this.getIfDate(date);
    if (this.doneCalender[resp.index]) {
        return this.doneCalender[resp.index].order_exists ? 'table-danger' : '';
    }
    return '';
}

function handleClick() {
    this.setSelectionDay(this.day);
    if (this.$gtm.enabled()) {
        this.$gtm.trackEvent({
            date: this.day,
        });
    }
    this.next();
}

export default {
    ...mapActions([
        'fetchCalender',
        'setSelectionDay',
    ]),
    dateDisabled,
    getIfDate,
    dateClass,
    handleClick,
};
