import {
    BContainer,
    BRow,
    BCol,
    BButton,
    BCalendar,
} from 'bootstrap-vue';

export default {
    BContainer,
    BRow,
    BCol,
    BButton,
    BCalendar,
};
