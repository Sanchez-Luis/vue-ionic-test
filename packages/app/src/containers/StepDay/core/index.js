import components from './components';
import data from './data';
import methods from './methods';
import computed from './computed';
import props from './props';
import watch from './watch';

export default {
    components,
    data,
    methods,
    computed,
    props,
    watch,
    async created() {
        await this.fetchCalender();
    },
};
