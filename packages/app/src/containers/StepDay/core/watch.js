export default {
    day(value) {
        let resp = false;
        for (let index = 0; index < this.doneCalender.length; index += 1) {
            if (value === this.doneCalender[index].day) {
                this.errorOrder = false;
                if (this.doneCalender[index].order_exists) {
                    resp = true;
                    this.errorOrder = true;
                }
            }
        }
        this.status = resp;
    },
    doneCalender() {
        let findFrist = '';
        if (this.doneCalender.length > 0) {
            for (let index = 0; index < this.doneCalender.length; index += 1) {
                if (!this.doneCalender[index].order_exists) {
                    findFrist = this.doneCalender[index].day;
                    break;
                }
            }
            this.day = findFrist;
        }
    },
};
