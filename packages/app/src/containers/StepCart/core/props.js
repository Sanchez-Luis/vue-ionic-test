export default {
    next: {
        type: Function,
    },
    back: {
        type: Function,
    },
    edit: {
        type: Function,
    },
};
