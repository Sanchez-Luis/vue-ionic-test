function handleClick() {
    if (this.doneEditStatus) {
        this.edit(this.observations);
    } else {
        this.next(this.observations);
    }
}

export default {
    handleClick,
};
