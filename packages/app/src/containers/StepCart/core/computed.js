import { mapGetters } from 'vuex';

export default {
    ...mapGetters([
        'doneSelectionDayPrint',
        'doneTimeSelected',
        'doneProductSelected',
        'doneTakeaway',
        'doneEditStatus',
    ]),
    total() {
        let count = 0;
        for (let index = 0; index < this.doneProductSelected.length; index += 1) {
            count += parseFloat(this.doneProductSelected[index].price, 10)
                    * parseInt(this.doneProductSelected[index].quantity, 10);
        }
        return count.toFixed(2);
    },
    textTypeOrder() {
        if (this.doneTakeaway) {
            return 'You selected to take away your order';
        }
        return 'You have 1 sit reserved';
    },
    handleTitleText() {
        return this.doneEditStatus ? '2. Cart' : '4. Cart';
    },
};
