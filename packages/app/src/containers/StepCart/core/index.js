import components from './components';
import data from './data';
import methods from './methods';
import props from './props';
import computed from './computed';

export default {
    components,
    data,
    methods,
    props,
    computed,
};
