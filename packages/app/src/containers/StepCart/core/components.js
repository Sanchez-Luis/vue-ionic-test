import {
    BContainer,
    BRow,
    BCol,
    BButton,
    BFormTextarea,
    BIcon,
    BIconPencilSquare,
} from 'bootstrap-vue';

import ProductDetail from '@i2tic-ikeafood/product-detail';
import BackButton from '../../../components/backButton/backButton.vue';

export default {
    BContainer,
    BRow,
    BCol,
    BButton,
    BFormTextarea,
    BackButton,
    ProductDetail,
    BIcon,
    BIconPencilSquare,
};
