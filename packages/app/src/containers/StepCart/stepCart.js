import componentConfig from './core';

export default {
    name: 'StepCart',
    ...componentConfig,
};
