import componentConfig from './core';

export default {
    name: 'Home',
    ...componentConfig,
};
