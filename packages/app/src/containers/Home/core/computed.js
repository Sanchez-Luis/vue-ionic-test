import { mapGetters } from 'vuex';

export default {
    ...mapGetters([
        'doneSelectionAllowed',
    ]),
};
