import {
    BContainer, BRow, BCol, BImg, BButton,
} from 'bootstrap-vue';

import LogOutButton from '../../../components/logOutButton/logOutButton.vue';

export default {
    BContainer,
    BRow,
    BCol,
    BImg,
    BButton,
    LogOutButton,
};
