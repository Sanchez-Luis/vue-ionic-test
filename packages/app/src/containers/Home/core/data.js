import ImageHero from '../../../static/img/bg-ikea-home-c.jpg';
import URL_CONSTANT from '../../../routes/urlConstant';

export default () => ({
    imgHero: ImageHero,
    URL_CONSTANT,
});
