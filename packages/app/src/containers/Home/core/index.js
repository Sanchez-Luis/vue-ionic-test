import components from './components';
import data from './data';
import methods from './methods';
import computed from './computed';

export default {
    components,
    data,
    methods,
    computed,
};
