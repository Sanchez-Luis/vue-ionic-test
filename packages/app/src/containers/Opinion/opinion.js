import componentConfig from './core';

export default {
    name: 'Opinion',
    ...componentConfig,
};
