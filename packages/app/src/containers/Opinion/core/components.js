import {
    BContainer,
    BRow,
    BCol,
    BButton,
    BFormTextarea,
} from 'bootstrap-vue';

import Feedback from '@i2tic-ikeafood/feedback';

export default {
    BContainer,
    BRow,
    BCol,
    BButton,
    BFormTextarea,
    Feedback,
};
