import { mapActions } from 'vuex';

function handleFeedback(e, questionNumber) {
    switch (questionNumber) {
    case 1:
        this.question1 = e + 1;
        break;
    default:
        break;
    }
}

async function sendFeedback() {
    const response = await this.createFeedback({ rating: this.question1, suggestions: this.feedbackText });
    let retValues = ['sad', 'dissatisfied', 'neutral', 'satisfied', 'happy'];
    if (response.status) {
        if (this.$gtm.enabled()) {
            if (this.question1 > 0 && this.question1 <= 5) {
                retValues = retValues[this.question1 - 1];
            }
            this.$gtm.trackEvent({
                feedback_selected: retValues,
            });
        }
        this.$router.push(this.URL_CONSTANT.home);
    }
}
export default {
    ...mapActions([
        'createFeedback',
    ]),
    handleFeedback,
    sendFeedback,
};
