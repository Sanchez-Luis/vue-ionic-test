import {
    getAccessToken,
    getRefreshToken,
    clearAccessToken,
    clearRefreshToken,
    saveAccessToken,
    saveRefreshToken,
} from '../services/localStorage/jwt';
import api from '../store/api/authentication_jwt';

export async function verifyTokenAction(access) {
    let verifyResponse = null;
    try {
        verifyResponse = await api.verifyJWT(access);
        if (!verifyResponse) {
            return null;
        }
    } catch (error) {
        return null;
    }
    return access;
}

export async function refreshTokenAction(refreshToken) {
    let verifyResponse = false;
    try {
        verifyResponse = await api.refreshJWT(refreshToken);
        if (!verifyResponse) {
            return false;
        }
        saveAccessToken(verifyResponse.access_token);
        saveRefreshToken(verifyResponse.refresh_token);
    } catch (error) {
        return false;
    }
    return true;
}

export function redirectToLogin() {
    window.location = '/oauth2/login';
}

export function redirectToLogOut() {
    window.location = '/oauth2/logout';
}

// eslint-disable-next-line max-statements
export async function verifyToken(to, from, next) {
    const accessToken = getAccessToken();
    const refreshToken = getRefreshToken();
    if (accessToken) {
        const response = await verifyTokenAction(accessToken);
        if (response) {
            next();
        } else {
            const responseRefresh = await refreshTokenAction(refreshToken);
            if (responseRefresh) {
                next();
            } else {
                clearToken();
            }
        }
    } else {
        redirectToLogin();
    }
}

function clearToken() {
    clearAccessToken();
    clearRefreshToken();
    redirectToLogin();
}
