import components from './components';
import methods from './methods';
import data from './data';

export default {
    components,
    methods,
    data,
    async mounted() {
        const response = await this.getTokens();

        if (response) {
            this.onVerifyAccessOk(response.access, response.refresh);
        } else {
            window.location = '/oauth2/login';
        }
    },
};
