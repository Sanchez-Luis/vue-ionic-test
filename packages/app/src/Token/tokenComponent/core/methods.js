import { verifyTokenAction } from '../../jwt';
import { saveAccessToken, saveRefreshToken } from '../../../services/localStorage/jwt';

function onVerifyAccessOk(access, refresh) {
    // save tokens

    saveAccessToken(access);
    saveRefreshToken(refresh);

    this.$router.push(this.URL_CONSTANT.home);
}

async function getTokens() {
    const { access, refresh } = this.$route.params;
    const response = await verifyTokenAction(access);
    if (response) {
        return { access, refresh };
    }
    return null;
}

export default {
    getTokens,
    onVerifyAccessOk,
};
