import componentConfig from './core';

export default {
    name: 'Token',
    ...componentConfig,
};
