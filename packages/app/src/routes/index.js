import Vue from 'vue';
import Router from 'vue-router';

import { getAccessToken } from '../services/localStorage/jwt';
import urlConstants from './url';

Vue.use(Router);

urlConstants.MAIN_LAYOUT.children = [
    urlConstants.HOME_PAGE_ROUTE,
    urlConstants.STEPPER_PAGE_ROUTE,
    urlConstants.ACTIVE_ORDERS,
    urlConstants.FEEDBACK,
    urlConstants.FAQ,
];

const router = new Router({
    mode: 'hash',
    routes: [
        // TOKEN_LAYOUT,
        urlConstants.TOKEN_ACCESS,
        urlConstants.MAIN_LAYOUT,
    ],
});

router.beforeEach((to, from, next) => {
    if (to.meta.requiresAuth && !getAccessToken() && to.name !== 'auth') {
        window.location = '/oauth2/login';
    } else {
        next();
    }
});

export default router;
