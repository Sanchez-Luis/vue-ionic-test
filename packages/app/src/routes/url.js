import URL_CONSTANT from './urlConstant';
import { verifyToken } from '../Token/jwt';

export default {
    HOME_PAGE_ROUTE: {
        name: 'Home',
        path: URL_CONSTANT.home,
        beforeEnter: verifyToken,
        component: () => import(
            /* webpackChunkName: "homePage" */
            '../containers/Home/home.vue'
        ),
    },
    STEPPER_PAGE_ROUTE: {
        name: 'Stepper',
        path: URL_CONSTANT.stepper,
        beforeEnter: verifyToken,
        alias: [
            URL_CONSTANT.stepperDay,
            URL_CONSTANT.stepperTime,
            URL_CONSTANT.stepperMeal,
            URL_CONSTANT.stepperCart,
            URL_CONSTANT.stepperFinal,
        ],
        component: () => import(
            /* webpackChunkName: "stepperPage" */
            '../containers/Stepper/stepper.vue'
        ),
    },
    TOKEN_ACCESS: {
        name: 'Token',
        path: URL_CONSTANT.token,
        component: () => import(
            '../Token/tokenComponent/token.vue'
        ),
    },
    ACTIVE_ORDERS: {
        name: 'ActiveOrders',
        path: URL_CONSTANT.activeOrders,
        beforeEnter: verifyToken,
        component: () => import(
            '../containers/ActiveOrders/activeOrders.vue'
        ),
    },
    FEEDBACK: {
        name: 'Feedback',
        path: URL_CONSTANT.feedback,
        beforeEnter: verifyToken,
        component: () => import(
            '../containers/Opinion/opinion.vue'
        ),
    },
    FAQ: {
        name: 'FAQ',
        path: URL_CONSTANT.faq,
        beforeEnter: verifyToken,
        component: () => import(
            '../containers/FAQ/faq.vue'
        ),
    },
    MAIN_LAYOUT: {
        path: '',
        beforeEnter: verifyToken,
        component: () => import(
            '../layout/main/main.vue'
        ),
    },
};
