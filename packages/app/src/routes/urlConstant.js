export default {
    home: '/',
    stepper: '/stepper',
    stepperDay: '/stepper-day',
    stepperTime: '/stepper-time',
    stepperMeal: '/stepper-meal',
    stepperCart: '/stepper-cart',
    stepperFinal: '/stepper-final',
    token: '/auth/:access/:refresh/',
    activeOrders: '/activeOrders',
    feedback: '/feedback',
    faq: '/faq',
};
