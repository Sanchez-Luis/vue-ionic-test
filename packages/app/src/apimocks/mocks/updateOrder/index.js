module.exports = (app, AJAX_TIMEOUT) => {
    app.post('/api/update_order/:id/', (_, response) => {
        setTimeout(() => {
            response.json({});
        }, AJAX_TIMEOUT);
    });
};
