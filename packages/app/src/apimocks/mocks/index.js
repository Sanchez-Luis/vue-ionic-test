const hoursMock = require('./hours');
const productsMock = require('./products');
const ordersMock = require('./orders');
const tokenMock = require('./verifyToken');
const activeOrdersMock = require('./activeOrders');
const updateOrderMock = require('./updateOrder');
const headerMock = require('./header');
const categoriesMock = require('./categories');
const feedbackMock = require('./feedback');
const calenderMock = require('./calender');
const modifyOrderMock = require('./modifyOrder');

const DEFAULT_AJAX_TIMEOUT = process.env.START_WITH_DELAY ? 1500 : 0;

// eslint-disable-next-line max-statements
module.exports = (app) => {
    hoursMock(app, DEFAULT_AJAX_TIMEOUT);
    productsMock(app, DEFAULT_AJAX_TIMEOUT);
    ordersMock(app, DEFAULT_AJAX_TIMEOUT);
    tokenMock(app, DEFAULT_AJAX_TIMEOUT);
    activeOrdersMock(app, DEFAULT_AJAX_TIMEOUT);
    updateOrderMock(app, DEFAULT_AJAX_TIMEOUT);
    headerMock(app, DEFAULT_AJAX_TIMEOUT);
    categoriesMock(app, DEFAULT_AJAX_TIMEOUT);
    feedbackMock(app, DEFAULT_AJAX_TIMEOUT);
    calenderMock(app, DEFAULT_AJAX_TIMEOUT);
    modifyOrderMock(app, DEFAULT_AJAX_TIMEOUT);
};
