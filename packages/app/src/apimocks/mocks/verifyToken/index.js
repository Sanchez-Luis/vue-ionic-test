module.exports = (app, AJAX_TIMEOUT) => {
    app.post('/api/token/verify/', (_, response) => {
        setTimeout(() => {
            response.json({});
        }, AJAX_TIMEOUT);
    });
};
