const categories = require('../../fixtures/categories.json');

module.exports = (app, AJAX_TIMEOUT) => {
    app.get('/api/categories/', (_, response) => {
        setTimeout(() => {
            response.json(categories);
        }, AJAX_TIMEOUT);
    });
};
