const activeOrders = require('../../fixtures/activeOrders.json');

module.exports = (app, AJAX_TIMEOUT) => {
    app.get('/api/active_orders/', (_, response) => {
        setTimeout(() => {
            response.json(activeOrders);
        }, AJAX_TIMEOUT);
    });
};
