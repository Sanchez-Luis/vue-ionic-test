const getCalender = require('../../fixtures/getCalender.json');

module.exports = (app, AJAX_TIMEOUT) => {
    app.get('/api/calendar/', (_, response) => {
        setTimeout(() => {
            response.json(getCalender);
        }, AJAX_TIMEOUT);
    });
};
