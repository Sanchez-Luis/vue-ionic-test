const productsFixtures = require('../../fixtures/products.json');

module.exports = (app, AJAX_TIMEOUT) => {
    app.get('/api/menu_day/:day/', (_, response) => {
        setTimeout(() => {
            response.json(productsFixtures);
        }, AJAX_TIMEOUT);
    });
};
