module.exports = (app, AJAX_TIMEOUT) => {
    app.post('/api/create_order/', (_, response) => {
        setTimeout(() => {
            response.json({
                success: true,
            });
        }, AJAX_TIMEOUT);
    });
};
