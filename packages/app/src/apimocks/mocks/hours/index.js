const hoursFixtures = require('../../fixtures/hours.json');

module.exports = (app, AJAX_TIMEOUT) => {
    app.get('/api/time_slots/:day/', (_, response) => {
        setTimeout(() => {
            response.json(hoursFixtures);
        }, AJAX_TIMEOUT);
    });
};
