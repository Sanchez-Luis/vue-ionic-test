module.exports = (app, AJAX_TIMEOUT) => {
    app.put('/api/modify_order/:id/', (_, response) => {
        setTimeout(() => {
            response.json({
                success: true,
            });
        }, AJAX_TIMEOUT);
    });
};
