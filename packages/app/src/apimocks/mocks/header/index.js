const headerFixtures = require('../../fixtures/header.json');

module.exports = (app, AJAX_TIMEOUT) => {
    app.get('/api/header/', (_, response) => {
        setTimeout(() => {
            response.json(headerFixtures);
        }, AJAX_TIMEOUT);
    });
};
