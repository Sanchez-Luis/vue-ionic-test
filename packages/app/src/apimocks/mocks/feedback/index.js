module.exports = (app, AJAX_TIMEOUT) => {
    app.post('/api/create_feedback/', (_, response) => {
        setTimeout(() => {
            response.json({
                success: true,
            });
        }, AJAX_TIMEOUT);
    });
};
