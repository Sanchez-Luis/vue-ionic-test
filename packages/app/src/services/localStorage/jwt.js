const ACCESS_TOKEN = 'ACCESS_TOKEN';
const REFRESH_TOKEN = 'REFRESH_TOKEN';

export function saveAccessToken(access) {
    localStorage.setItem(ACCESS_TOKEN, access);
}

export function saveRefreshToken(access) {
    localStorage.setItem(REFRESH_TOKEN, access);
}

export function getAccessToken() {
    return localStorage.getItem(ACCESS_TOKEN);
}

export function getRefreshToken() {
    return localStorage.getItem(REFRESH_TOKEN);
}

export function clearAccessToken() {
    localStorage.clear(ACCESS_TOKEN);
}

export function clearRefreshToken() {
    localStorage.clear(REFRESH_TOKEN);
}
