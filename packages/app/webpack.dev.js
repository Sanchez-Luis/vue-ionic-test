const webpackDevelopmentConfig = require('../../webpack.dev');
const mocks = require('./src/apimocks/mocks');

const devServerConf = {}
if (process.env.IS_DB_LOCAL) {
    devServerConf.proxy = {
        "/api/*": {
            target: "http://localhost:8000",
            secure: false
        }
    }
} else {
    devServerConf.before = (app) => mocks(app);
    devServerConf.contentBase = './example';
    devServerConf.port = 8080;
    devServerConf.writeToDisk = true;
}

module.exports = webpackDevelopmentConfig({
    entry: {
        index: ['./example/index.js'],
    },
    output: {
        library: 'App',
    },
    devServer: {
        ...devServerConf
    },
    devtool: 'source-map',
});
