const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpackProductionConfig = require('../../webpack.prod');

module.exports = webpackProductionConfig({
    output: {
        // An absolute path to the desired output directory.
        path: path.resolve(__dirname, 'dist'),

        // A filename pattern for the output files
        filename: '[name].js',

        // IMPORTANT!: This is the name of the global variable exported in browsers
        // Change it for the name you want your component to have as window.NAME
        library: 'App',

        libraryTarget: 'umd',
    },
    entry: {
        index: [path.resolve(__dirname, 'index.js')],
    },
    plugins: [new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'index.html'),
    })]
});
