/* eslint-disable import/no-extraneous-dependencies */
import Vue from 'vue';
import { BootstrapVue } from 'bootstrap-vue';
import router from './src/routes';
import store from './src/store';
import App from './src/App.vue';
import Gtm from './src/gtm/gtmConfig';
import Cookies from './src/gtm/cookiesConfig';

import '@i2tic-ikeafood/ikea-styles';

function render() {
    new Vue({
        render: (renderElement) => renderElement(App),
        router,
        store,
        BootstrapVue,
        Gtm,
        Cookies,
    }).$mount('#app-ikeafood');
}

export default {
    render,
};
