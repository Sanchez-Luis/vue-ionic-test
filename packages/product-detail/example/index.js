/* eslint-disable import/no-extraneous-dependencies */
import renderHelper from '@globalHelpers/render';

import App from '../src/App.vue';

// import ExampleImage from './static/img/exampleProduct.jpg';
import ExampleImage from './static/img/exampleProduct.jpg';

const props = {
    dataProduct: {
        name: 'Pasta',
        price: 6,
        quantity: 0,
        picture: ExampleImage,
    },
    handleChange: (result) => {
        props.dataProduct.quantity = result;
    },
};

renderHelper(App, {
    elementToRender: '#i2tic-example',
    props,
});
