/* eslint-env node, jest */
/* eslint-disable import/no-extraneous-dependencies */
import { mount } from '@vue/test-utils';

import App from '../src/App.vue';

describe('ProductDetail', () => {
    let initialProps;
    let wrapper;

    beforeEach(() => {
        initialProps = {
            dataProduct: {
                name: 'TestTitle',
                price: 6,
                quantity: 0,
                picture: '',
            },
        };

        wrapper = mount(App, {
            propsData: {
                ...initialProps,
            },
        });
    });

    it('HTML Snapshot', () => {
        expect(wrapper.html()).toMatchSnapshot();
    });

    describe('When component is loaded', () => {
        it('Then render an H4 element with text comming from props', () => {
            const h4Element = wrapper.find('h4');
            expect(h4Element.text()).toBe('TestTitle');
        });

        it('Then render Price number of product with the price comming from props', () => {
            const pElement = wrapper.find('p');
            expect(pElement.text()).toBe('6 €');
        });
    });

});
