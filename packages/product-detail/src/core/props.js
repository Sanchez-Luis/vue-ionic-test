export default {
    dataProduct: {
        type: Object,
        default: () => ({
            name: 'Default',
            price: 0,
            picture: '',
            quantity: 0,
        }),
    },
    availability: {
        type: Boolean,
        default: true,
    },
};
