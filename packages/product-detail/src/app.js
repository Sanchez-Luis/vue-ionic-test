import componentConfig from './core';

export default {
    name: 'ProductDetail',
    ...componentConfig,
};
