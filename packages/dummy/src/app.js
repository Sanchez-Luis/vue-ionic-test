import componentConfig from './core';

export default {
    name: 'Dummy',
    ...componentConfig,
};
