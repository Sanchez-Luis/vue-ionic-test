/* eslint-disable import/no-extraneous-dependencies */
import renderHelper from '@globalHelpers/render';

import App from '../src/App.vue';

const props = {

};

renderHelper(App, {
    elementToRender: '#i2tic-example',
    props,
});
