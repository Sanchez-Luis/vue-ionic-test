/* eslint-env node, jest */
/* eslint-disable import/no-extraneous-dependencies */
import { mount } from '@vue/test-utils';

import App from '../src/App.vue';

const TIME_OUT = 500;

describe('Dummy', () => {
    let initialProps;
    let wrapper;

    beforeEach(() => {
        initialProps = {

        };

        wrapper = mount(App, {
            propsData: {
                ...initialProps,
            },
        });
    });

    it('HTML Snapshot', () => {
        expect(wrapper.html()).toMatchSnapshot();
    });

    describe('When component is loaded', () => {
        it('Then render an H1 element with default text', () => {
            const h1Element = wrapper.find('h1');

            expect(h1Element.text()).toBe('Click here (0 times)');
        });
    });

    describe('When click on H1 element', () => {
        it('The text changes by increasing its value by 1', (done) => {
            const h1Element = wrapper.find('h1');
            h1Element.trigger('click');

            setTimeout(() => {
                expect(h1Element.text()).toBe('Click here (1 times)');

                done();
            }, TIME_OUT);
        });
    });
});
