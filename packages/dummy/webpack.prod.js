const path = require('path');

const webpackProductionConfig = require('../../webpack.prod');

module.exports = webpackProductionConfig({
    output: {
        // An absolute path to the desired output directory.
        path: path.resolve(__dirname, 'dist/'),

        // A filename pattern for the output files
        filename: '[name].js',

        // IMPORTANT!: This is the name of the global variable exported in browsers
        // Change it for the name you want your component to have as window.NAME
        library: 'Dummy',

        libraryTarget: 'umd',
    },
    entry: {
        index: [path.resolve(__dirname, 'src/App.vue')],
    },
});
