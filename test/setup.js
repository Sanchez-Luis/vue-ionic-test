/* eslint-env node, jest */
import fetchMock from 'jest-fetch-mock';
import { mount } from '@vue/test-utils';

fetchMock.enableMocks();

const div = document.createElement('div');
div.id = 'hello-world-app';

document.body.appendChild(div);
global.globalMount = mount;
